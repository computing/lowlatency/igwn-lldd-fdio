import logging
import multiprocessing as mp
import os
import shutil
import tempfile
import time
from pathlib import Path

import pytest

from igwn_lldd_fdio.fdio2kafka import main as fdio2kafka
from igwn_lldd_fdio.kafka2fdio import main as kafka2fdio

logging.basicConfig(level=logging.DEBUG)

KAFKA_TOPIC = 'TestTopic'
TEST_DATA_DIR = Path(__file__).parent / 'data'
TEST_GWF_FILES = [
    'Z-igwn_lldd_common_test-1000000000-1.gwf',
    'Z-igwn_lldd_common_test-1000000001-1.gwf',
    'Z-igwn_lldd_common_test-1000000002-1.gwf',
]
PRODUCER_RUNTIME = 5
OBSERVATORY = 'Z'
INPUTPRODUCERCONFIG = Path(__file__).parent / 'TestFdIOKafkaProducer.cfg.in'
INPUTCONSUMERCONFIG = Path(__file__).parent / 'TestFdIOKafkaConsumer.cfg.in'


@pytest.mark.skipif(
    os.getenv('KAFKA_BROKER') is None,
    reason='There is not a active Kafka Broker',
)
def test_fdio2kafka():

    # Enable the coverage info to be collected from multiprocessing process
    try:
        from pytest_cov.embed import cleanup_on_sigterm
    except ImportError:
        pass
    else:
        cleanup_on_sigterm()

    # Create temp config files
    producerconfig = tempfile.NamedTemporaryFile(
        mode='wt', prefix='producer', suffix='.cfg'
    )
    consumerconfig = tempfile.NamedTemporaryFile(
        mode='wt', prefix='consumer', suffix='.cfg'
    )

    with tempfile.TemporaryDirectory() as indirname:
        with tempfile.TemporaryDirectory() as outdirname:

            # Read the input config file
            with open(INPUTCONSUMERCONFIG) as f:
                consumerconfigcontents = f.read()

            # Modify its contents
            outputfileprefix = os.path.join(
                outdirname, f'{OBSERVATORY}-{KAFKA_TOPIC}'
            )
            consumerconfigcontents = consumerconfigcontents.replace(
                'OUTFILEPREFIX', outputfileprefix
            )
            consumerconfigcontents = consumerconfigcontents.replace(
                'BOOTSTRAPSERVERS', os.getenv('KAFKA_BROKER')
            )
            consumerconfigcontents = consumerconfigcontents.replace(
                'TOPICNAME', KAFKA_TOPIC
            )

            # Write it out and flush its contents
            consumerconfig.write(consumerconfigcontents)
            consumerconfig.flush()

            # Have the arguments point to this config file
            consumer_args = ['--config', consumerconfig.name]

            # Setup the consumer using the kafka2fdio script
            consumer = mp.Process(target=kafka2fdio, args=(consumer_args,))

            # Start the consumer
            consumer.start()

            time.sleep(5)

            # Read the input config file
            with open(INPUTPRODUCERCONFIG) as f:
                producerconfigcontents = f.read()

            # Modify its contents
            producerconfigcontents = producerconfigcontents.replace(
                'INPUTDIR', indirname
            )
            producerconfigcontents = producerconfigcontents.replace(
                'BOOTSTRAPSERVERS', os.getenv('KAFKA_BROKER')
            )
            producerconfigcontents = producerconfigcontents.replace(
                'TOPICNAME', KAFKA_TOPIC
            )

            # Write it out and flush its contents
            producerconfig.write(producerconfigcontents)
            producerconfig.flush()

            # Have the arguments point to this config file
            producer_args = ['--config', producerconfig.name]

            # Setup the producer using the fdio2kafka script
            producer = mp.Process(target=fdio2kafka, args=(producer_args,))

            # Start the producer
            producer.start()

            # Allow the producer to startup before writing frames
            time.sleep(5)

            # Loop over each of the test files reading them and
            # writing them to the tmp directory so they can be picked
            # up by the fdio2kafka process
            for ifile in TEST_GWF_FILES:

                # Copy the file to the dest directory so it can be picked
                # up by the producer
                inputfile = TEST_DATA_DIR / ifile
                destfile = os.path.join(indirname, ifile)

                # Copy the file
                shutil.copyfile(inputfile, destfile)

                # Wait 1 second so the file comes in every 1 second
                time.sleep(1)

            # Wait for the producer to stop
            producer.join()

            # Wait for the consumer to stop
            consumer.join()

            print(os.listdir(indirname))
            print(os.listdir(outdirname))

            for ifile in TEST_GWF_FILES:

                # Set the filenames to check the file contents
                infile = os.path.join(indirname, ifile)
                ofile = ifile.replace(
                    'Z-igwn_lldd_common_test', f'{OBSERVATORY}-{KAFKA_TOPIC}'
                )
                outfile = os.path.join(outdirname, ofile)

                # Check that the infile and outfile
                assert os.path.exists(infile), f'The copy of {infile} failed'
                assert os.path.exists(outfile), (
                    'The consumer did not ' f'write {outfile}'
                )

                # # Compare the contents of the generated files
                # assert filecmp.cmp(infile, outfile), "The contents for " \
                #     f"the {infile} and {outfile} do not match"

    producerconfig.close()
    consumerconfig.close()
