import io
import logging

import configargparse
import PyFd as fd

logger = logging.getLogger(__name__)


# Override the config file parser class parse function to be able to handle
# FdIO keywords in the config file
class FdIOConfigFileParser(configargparse.DefaultConfigFileParser):
    def parse(self, stream):

        # Remove FdIO keywords from the config file
        buffer = ''
        for line in stream:

            # Check line does not contain FdIO keywords
            if (
                line[:4] != 'FDIN'
                and line[:5] != 'FDOUT'
                and line[:3] != 'CFG'
                and 'FD_NO_CONFIG_CHECK' not in line
            ):
                buffer += line

        # Open up a new text stream to write to
        newstream = io.StringIO(buffer)

        # Pass this new stream to the super class parse function
        items = super().parse(newstream)

        return items


def check_frame_buffer(frame_buffer):

    if frame_buffer[39] != 1:
        logger.warn('No file CRC check present')
        return 1

    # Do a check of the buffer before reading
    nframes = fd.FrameBufCheck(frame_buffer, len(frame_buffer), True)

    # Check if buffer check failed
    if nframes < 0:
        if nframes == -3:
            logger.warn('Unable to allocate memory for the file')
        elif nframes == -4:
            logger.warn('Unable to open the file')
        elif nframes == -5:
            logger.warn(
                'nBytes reported in the file is not the same as'
                'its actual buffer size'
            )
        elif nframes == -6:
            logger.warn('Header checksum failed')
        elif nframes == -7:
            logger.warn('File checksum failed')
        else:
            logger.warn(
                'An unknown error occured when checking' 'the frame buffer'
            )

        return 2

    return 0
