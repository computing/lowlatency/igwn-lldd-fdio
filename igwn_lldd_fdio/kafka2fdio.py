#!/usr/bin/python3

# -*- coding: utf-8 -*-
# Copyright (C) European Gravitational Observatory (EGO) (2022)
#
# Author: Rhys Poulton <poulton@ego-gw.it>
#
# This file is part of igwn-lldd.
#
# igwn-lldd is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# igwn-lldd is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with igwn-lldd.  If not, see <http://www.gnu.org/licenses/>.

import logging
import sys
import time

import configargparse
import PyFd as fd
import virgotools as vt
from igwn_lldd_common.framekafkaconsumer import FrameKafkaConsumer
from igwn_lldd_common.utils import parse_topics, str2bool

from .utils import FdIOConfigFileParser, check_frame_buffer

logger = logging.getLogger(__name__)


def main(args=None):

    parser = configargparse.ArgumentParser(
        config_file_parser_class=FdIOConfigFileParser
    )
    parser.add_argument(
        'config',
        nargs='?',
        is_config_file=True,
        help='config file path',
    )
    parser.add_argument(
        '-d', '--debug', type=int, help='increase debug level (default 0=off)'
    )
    parser.add_argument(
        '-maxt',
        '--max-runtime',
        type=float,
        help='Maximum time to run before exiting (undef=Infinity)',
    )
    parser.add_argument(
        '-dw',
        '--debug-wait',
        type=float,
        default=0.0,
        help='wait x seconds between gwf files (used to intentionally break \
things for debugging)',
    )
    parser.add_argument(
        '-b',
        '--bootstrap-servers',
        type=str,
        default='localhost:9092',
        help='specify the Kafka cluster bootstrap servers',
    )
    parser.add_argument(
        '-a',
        '--add-topic-partition',
        type=str,
        action='append',
        required=True,
        help='/topic=LHO_Data/delta-t=4/crc-check=true/max-latency=60/\
fast-forward-buffer=4/acceptable-latency=8/',
    )
    parser.add_argument(
        '-crc',
        '--crc-check',
        action='store_const',
        const=True,
        default=False,
        help='Run a CRC check for each frame',
    )
    parser.add_argument(
        '-x',
        '--exit-if-missing-topics',
        action='store_const',
        const=True,
        default=False,
        help='exit if any topics are missing',
    )
    parser.add_argument(
        '-s',
        '--ssl',
        action='store_const',
        const=True,
        default=False,
        help='use ssl',
    )
    parser.add_argument('-p', '--ssl-password', type=str, help='ssl password')
    parser.add_argument(
        '-ca', '--ssl-cafile', type=str, help='location of ca-cert'
    )
    parser.add_argument(
        '-cf',
        '--ssl-certfile',
        type=str,
        help='location of signed certificate',
    )
    parser.add_argument(
        '-kf', '--ssl-keyfile', type=str, help='location of personal keyfile'
    )
    parser.add_argument('-g', '--group-id', type=str, help='Kafka group ID')
    parser.add_argument(
        '-su',
        '--status-updates',
        action='store_const',
        const=True,
        default=False,
        help='store status updates',
    )
    parser.add_argument(
        '-st', '--status-topic', type=str, help='topic name for status updates'
    )
    parser.add_argument(
        '-sb',
        '--status-bootstrap',
        type=str,
        help='specify the kafka cluster for status updates',
    )
    parser.add_argument(
        '-si',
        '--status-interval',
        type=int,
        default=60,
        help='interval in seconds between status updates',
    )
    parser.add_argument(
        '-sn',
        '--status-nodename',
        type=str,
        help='specify the node name used in status updates',
    )
    parser.add_argument(
        '-ff',
        '--fast-forward',
        type=str2bool,
        default=True,
        help='fast forward if fall behind',
    )
    # custom libraries
    parser.add_argument(
        '-lkp',
        '--load-kafka-python',
        action='store_const',
        const=True,
        default=False,
        help='load kafka-python rather than confluent-kafka',
    )
    parser.add_argument(
        '-pt',
        '--poll-timeout',
        type=int,
        default=1000,
        help='Timeout when doing consumer.poll() [in ms]. Default: 1000.',
    )
    parser.add_argument(
        '-pr',
        '--poll-max-records',
        type=int,
        default=1,
        help='Max records returned when doing consumer.poll(). Default: 1.',
    )
    parser.add_argument(
        '-v',
        '--verbose',
        default=False,
        const=True,
        action='store_const',
        help='verbose log output',
    )

    # Parse the arguments from the command line
    args, unknown = parser.parse_known_args(args)

    # Put the logging into the expected format
    vt.log_to_cfg(level=logging.INFO)

    # Start the fdio server
    fdio = vt.FdIO(['', args.config])

    # Mark the server as configured
    fd.CfgReachState(fd.CfgServerConfigured)

    # Get the topics from the topic partition
    tp_info = parse_topics(args.add_topic_partition)

    # Startup the frame kafka consumer
    framekafkaconsumer = FrameKafkaConsumer(args, tp_info)

    # Mark the server as golden
    fd.CfgReachState(fd.CfgServerGolden)

    # Mark start time
    start_time = time.time()

    while not fd.CfgFinished():

        # Check if the runtime has gone above the max runtime
        runtime = time.time() - start_time
        if args.max_runtime and runtime > args.max_runtime:
            logger.info('Have run for %f seconds stopping' % runtime)
            break

        # Run consumer.poll()
        r_poll = framekafkaconsumer.poll_consumer_for_topic()

        if r_poll == {}:
            # Update program state and add message if frame missing
            dt = int(time.time() - fdio._fdio.contents.lastFrameTime)
            if (
                dt
                > fdio._fdio.contents.noFrMsgTimeOut
                + 2 * fdio._fdio.contents.lastIFrameDt
            ):
                if fdio._fdio.contents.getFrStatus == fd.FR_YES:
                    fd.CfgMsgAddWarning(
                        'No input frame since at least %d seconds', dt
                    )
                fdio._fdio.contents.getFrStatus = fd.FR_NO
                fd.CfgMsgAddUserInfo('Warning: no input frame since %d s', dt)
                fd.CfgMsgSendWithTimeout(0.001)

                # Update the state of the server to be active (not recieving)
                if fd.CfgGetState() != fd.CfgServerActive:
                    fd.CfgReachState(fd.CfgServerActive)
        else:
            # Update the server to golden (processing frames)
            if fd.CfgGetState() != fd.CfgServerGolden:
                fd.CfgReachState(fd.CfgServerGolden)

        # parse the messages
        for topic_partition in r_poll:
            for message in r_poll[topic_partition]:

                # Check if this program has been asked to exit
                if fd.CfgFinished():
                    logger.info('main: exiting while processing messages')

                # Get the frame buffer from the kafka messgae
                (
                    frame_buffer,
                    payload_info,
                ) = framekafkaconsumer.extract_frame_buffer_from_message(
                    message, tp_info
                )

                # Keep the FdIOServer Alive
                if not fdio._fdio.contents.userInfo:
                    fd.CfgMsgAddUserInfo(
                        'GPS: %i, File Timestamp: %i'
                        % (
                            vt.now_gps(),
                            payload_info['data_gps_timestamp'],
                        )
                    )
                    fd.CfgMsgSendWithTimeout(0.001)

                # Continue if the full frame has not been assembled
                if not frame_buffer:
                    continue

                # See if we should check the file CRC checksum
                if args.crc_check:
                    # Check the frame buffer before reading it
                    ret = check_frame_buffer(frame_buffer)
                    if ret != 0:
                        logger.warn(
                            'CRC check failed at '
                            f"{payload_info['data_gps_timestamp']}, skipping"
                        )

                # Extract the frame from the payload
                frame = fd.FdIOGetFrameFromBuf(
                    fdio._fdio, frame_buffer, len(frame_buffer), -1
                )

                # Put the kafka latency in the (SMS) serData in the frame and
                # in the UserInfo to be printed in the VirgoProcessMonitor
                # (VPM) by looping over the actionsIn
                action = fdio._fdio.contents.actionsIn
                while action:

                    if action.contents.type == b'Stat':

                        # Modify the serData (SMS) in the frame to add the
                        # kafka latency
                        serData = fd.String(
                            bytes(
                                'kafkaLatency '
                                f"{payload_info['kafka_latency']:.2f}",
                                'utf-8',
                            )
                        )
                        action.contents.serData = serData
                        action.contents.serDataSize = len(serData) + 2

                        # Modify the UserInfo to be printed on the VPM
                        # to include the kafka latency
                        userInfo = fd.String(
                            bytes(
                                'kafkaLatency '
                                f"{payload_info['kafka_latency']:.2f}",
                                'utf-8',
                            )
                        )
                        action.contents.userInfo = userInfo
                        action.contents.userInfoSize = len(userInfo) + 2
                        break

                    # Move to the next action
                    action = action.contents.next

                # Output the frame
                fd.FdIOPutFrame(fdio._fdio, frame)

                if args.debug_wait > 0.0:
                    logger.info('WAITING...')
                    time.sleep(args.debug_wait)

    # and close the Kafka connection
    framekafkaconsumer.close()


if __name__ == '__main__':
    sys.exit(main())
